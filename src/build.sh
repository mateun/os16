# first we assemble the bootsector:
nasm bootsect.asm -o ../build/bootsect.bin

# next we create an floppy image:
dd if=/dev/zero of=../build/bootdisk.bin bs=512 count=2880

# then we write our bootsector to this (floppy) image file:
dd if=../build/bootsect.bin of=../build/bootdisk.bin conv=notrunc

